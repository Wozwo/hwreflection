﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace HWReflection
{
    class LoggingTime
    {
        public static void TimeLogging(int length, TypeSerialize typeSerialize, Stopwatch stopWatch, bool serializeOrDeserialize)
        {
            Console.WriteLine($"\nВыполнена {(serializeOrDeserialize ? "сериализация" : "десериализация")}." +
                $"\nКол-во интераций: {length}" +
                $"\nИспользуемая реализация: {((typeSerialize == TypeSerialize.Reflection) ? "Рефлексия" : "NewtonJSon")}" +
                $"\nЗатраченное время: {stopWatch.Elapsed.TotalMilliseconds}");
        }
    }
}
