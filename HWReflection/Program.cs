﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace HWReflection
{
    class Program
    {
        static Stopwatch stopWatch = new Stopwatch();
        static void Main(string[] args)
        {
            string answer;
            object data;
            

            answer = StartSerialize(100, Data.GetData(), TypeSerialize.Reflection);
            answer = StartSerialize(100_000, Data.GetData(), TypeSerialize.Reflection);
            answer = StartSerialize(100, Data.GetData(), TypeSerialize.Json);
            answer = StartSerialize(100_000, Data.GetData(), TypeSerialize.Json);

            //Десериализация
            var deserializeCSV = "i1 = 11\ni2 = 21\ni3 = 31\ni4 = 41\ni5 = 51";
            var deserializeJson = @"{""i1"":61,""i2"":62,""i3"":63,""i4"":64,""i5"":65}";

            data = StartDeserialize(100, deserializeCSV, TypeSerialize.Reflection);
            data = StartDeserialize(100_000, deserializeCSV, TypeSerialize.Reflection);
            data = StartDeserialize(100, deserializeJson, TypeSerialize.Json);
            data = StartDeserialize(100_000, deserializeJson, TypeSerialize.Json);

            OutputResultSerialize();
            Console.ReadKey();
        }

        static string StartSerialize(int length, object data, TypeSerialize typeSerialize)
        {
            stopWatch.Reset();
            stopWatch.Start();
            string csv = null;
            for (int i = 0; i < length; i++)
            {
                if (typeSerialize == TypeSerialize.Reflection)
                    csv = Serialize.SerializeReflectionToCSV(data);
                else if(typeSerialize == TypeSerialize.Json)
                    csv = Serialize.SerializeJson(data);
            }
            stopWatch.Stop();
            LoggingTime.TimeLogging(length, typeSerialize, stopWatch, true);
            return csv;
        }

        static object StartDeserialize(int length, string stringData, TypeSerialize typeSerialize)
        {
            stopWatch.Reset();
            stopWatch.Start();
            object data = null;
            for (int i = 0; i < length; i++)
            {
                if (typeSerialize == TypeSerialize.Reflection)
                    data = Serialize.DeserializeReflection(stringData);
                else if (typeSerialize == TypeSerialize.Json)
                    data = Serialize.DeserializeJSon(stringData);
            }
            stopWatch.Stop();
            LoggingTime.TimeLogging(length, typeSerialize, stopWatch, false);
            return data;
        } 

        static void OutputResultSerialize()
        {
            var csv = Serialize.SerializeReflectionToCSV(Data.GetData());
            stopWatch.Reset();
            stopWatch.Start();
            Console.WriteLine("\n" + csv);
            stopWatch.Stop();
            Console.WriteLine($"\nВремя затраченное на вывод сериализованных данных: {stopWatch.Elapsed.TotalMilliseconds}");
        }
    }
}
