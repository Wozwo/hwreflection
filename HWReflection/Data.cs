﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HWReflection
{
    class Data
    {
        public int i1, i2, i3, i4, i5;

        public static Data GetData()
        {
            return new Data() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
        }
    }
}
