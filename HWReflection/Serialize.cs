﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace HWReflection
{
    class Serialize
    {
        //Сериализация с помощью рефлексии
        public static string SerializeReflectionToCSV(object data)
        {
            if (data != null)
            {
                var prop = data.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                var csv = string.Join("\n", prop.Select(x => $"{x.Name} = {x.GetValue(data)}"));
                return csv;
            }
            else
                return null;
        }

        //Сериализация с помощью newtonjson
        public static string SerializeJson(object data)
        {
            if (data != null)
            {
                var js = JsonConvert.SerializeObject(data);
                return js;
            }
            else
                return null;
        }

        //Десериализация с помощью рефлексии
        public static object DeserializeReflection(string csv)
        { 
            var pattern = @"(?<b>i.?)[\s]=[\s](?<a>.+)";
            var dataCsv = Regex.Match(csv, pattern);
            var data = Data.GetData();
            var des = data.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
            while (dataCsv.Success)
            {
                des.FirstOrDefault(x => x.Name == dataCsv.Groups["b"].ToString()).SetValue(data, int.Parse(dataCsv.Groups["a"].ToString()));
                dataCsv = dataCsv.NextMatch();
            }
            return data;
        }

        //Десериализация с помощью newtonjson
        public static object DeserializeJSon(string json)
        {
            var data = JsonConvert.DeserializeObject<Data>(json);
            return data;
        }
    }
}
